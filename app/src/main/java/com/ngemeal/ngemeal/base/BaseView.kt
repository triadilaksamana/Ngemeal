package com.ngemeal.ngemeal.base

interface BaseView {
    fun showLoading()
    fun dismissLoading()
}